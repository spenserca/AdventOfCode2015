'use strict';

// 214 too high, 186 too low
let placesVisited = [];

module.exports = (input) => {
    const locationDistances = input.split('\n')
        .map(i => {
            let split = i.split(' ');
            return {
                currentLocation: split[0],
                destination: split[2],
                distance: parseInt(split[4])
            }
        });

    let total = locationDistances.reduce((sum, current) => {
        let minDistance = 0;

        if (!locationHasBeenVisited(current[0], current[2])) {
            let distancesToTravel = locationDistances.filter(location => location[0] === current[0])
                .map(d => parseInt(d[4]));

            minDistance = Math.min(...distancesToTravel);;
        }

        placesVisited.push(current[0]);

        return sum + minDistance;
    }, 0);

    return total;
}

const locationHasBeenVisited = (currentLocation, destination) => {
    return placesVisited.includes(currentLocation) || placesVisited.includes(destination);
};