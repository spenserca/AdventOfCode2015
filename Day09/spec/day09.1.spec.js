'use strict';

const day091 = require('../day09.1');
const input =
    `London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141`;

xdescribe('Day09.1', function () {

    it('should return 605 for the given input', function () {
        expect(day091(input)).toEqual(605);
    });
});