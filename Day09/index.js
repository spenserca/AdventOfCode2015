'use strict';

const day091 = require('./day09.1');
const fs = require('fs');
const input = fs.readFileSync('./Day09/input.txt', 'utf8');

module.exports = () => {
    return `Day09.1: ${day091(input)}.`;
}