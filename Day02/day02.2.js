'use strict';

module.exports = function (input) {
    const dimensions = input.split('\n')
        .map(d => d.split('x')
            .map(v => parseInt(v)));
    let total = 0;

    dimensions.forEach(dimensionSet => {
        total += getSmallestPerimeter(dimensionSet) + getVolume(dimensionSet);
    });

    return total;
};

function getSmallestPerimeter(dimensions) {
    return Math.min((2 * dimensions[0] + 2 * dimensions[1]), (2 * dimensions[1] + 2 * dimensions[2]), (2 * dimensions[2] + 2 * dimensions[0]));
}

function getVolume(dimensions) {
    return dimensions[0] * dimensions[1] * dimensions[2];
}