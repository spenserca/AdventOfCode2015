'use strict';

const day022 = require('../day02.2');

describe('Day02.2', function () {

    it('should return 34 for 2x3x4', function () {
        expect(day022('2x3x4')).toEqual(34);
    });

    it('should return 14 for 1x1x10', function () {
        expect(day022('1x1x10')).toEqual(14);
    });
});