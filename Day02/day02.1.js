'use strict';

module.exports = function (input) {
    const dimensions = input.split('\n')
        .map(d => d.split('x')
            .map(n => parseInt(n)));
    let total = 0;

    dimensions.forEach(dimensionSet => {
        total += getSurfaceArea(dimensionSet) + getSmallestSideArea(dimensionSet);
    });

    return total;
};

function getSurfaceArea(dimensions) {
    return (2 * dimensions[0] * dimensions[1]) + (2 * dimensions[1] * dimensions[2]) + (2 * dimensions[2] * dimensions[0]);
}

function getSmallestSideArea(dimensions) {
    return Math.min((dimensions[0] * dimensions[1]), (dimensions[1] * dimensions[2]), (dimensions[2] * dimensions[0]));
}