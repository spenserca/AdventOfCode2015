'use strict';

const day01 = require('./Day01/index');
const day02 = require('./Day02/index');
const day03 = require('./Day03/index');
const day04 = require('./Day04/index');
const day05 = require('./Day05/index');
const day06 = require('./Day06/index');

const day08 = require('./Day08/index');
const day09 = require('./Day09/index');

const day12 = require('./Day12/index');

const day14 = require('./Day14/index');

console.log(day01());
console.log(day02());
console.log(day03());
console.log(day04());
console.log(day05());
console.log(day06());

console.log(day08());
console.log(day09());

console.log(day12());

console.log(day14());

process.exit();