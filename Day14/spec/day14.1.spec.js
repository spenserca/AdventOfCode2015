'use strict';

const day141 = require('../day14.1');

describe('Day14.1', function () {

    it('should return 1120 for given input', function () {
        let input = `Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\nDancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.`
        expect(day141(input, 1000)).toEqual(1120);
    });
});