'use strict';

const day142 = require('../day14.2');

xdescribe('Day14.2', function () {

    it('should return 689 for given input', function () {
        let input = `Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\nDancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.`
        expect(day142(input, 1000)).toEqual(689);
    });
});