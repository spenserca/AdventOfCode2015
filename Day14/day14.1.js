'use strict';

module.exports = (input, seconds) => {
    const reindeerStats = input.split('\n')
        .map(statLine => statLine.split(' '))
        .map(stats => {
            return {
                name: stats[0],
                topSpeed: parseInt(stats[3]),
                flightDuration: parseInt(stats[6]),
                restDuration: parseInt(stats[13]),
                cycleDuration: parseInt(stats[6]) + parseInt(stats[13])
            };
        });

    let distancesTravelled = reindeerStats.map(stat => {
        const fullCycles = parseInt(seconds / stat.cycleDuration);
        const fullCycleDistance = fullCycles * stat.topSpeed * stat.flightDuration;
        const remainingTime = seconds % stat.cycleDuration;
        return remainingTime >= stat.flightDuration
            ? fullCycleDistance + (stat.flightDuration * stat.topSpeed)
            : fullCycleDistance + (remainingTime * stat.topSpeed);
    });

    return Math.max(...distancesTravelled);
}