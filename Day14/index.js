'use strict';

const day141 = require('./day14.1');
const fs = require('fs');
const input = fs.readFileSync('./Day14/input.txt', 'utf8');

module.exports = () => {
    return `Day14.1: ${day141(input, 2503)}.`;
}