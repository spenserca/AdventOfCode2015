'use strict';

module.exports = (input, seconds) => {
    let reindeerStats = input.split('\n')
        .map(statLine => statLine.split(' '))
        .map(stats => {
            return {
                name: stats[0],
                topSpeed: parseInt(stats[3]),
                flightDuration: parseInt(stats[6]),
                restDuration: parseInt(stats[13]),
                cycleDuration: parseInt(stats[6]) + parseInt(stats[13]),
                isFlying: true,
                points: 0,
                totalDistance: 0
            };
        });

    // seconds.forEach(second => {
    //     reindeerStats = reindeerStats.map(reindeer => {
    //         if (reindeer.isFlying) {
    //             reindeer.totalDistance += topSpeed;
    //         }

    //         reindeer.isFlying = 
    //     });
    // });

    return 0;
}