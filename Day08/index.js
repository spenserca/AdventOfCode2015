'use strict';

const day081 = require('./day08.1');
const day082 = require('./day08.2');
const fs = require('fs');
const list = fs.readFileSync('./Day08/input.txt', 'utf8');

module.exports = () => {
    return `Day08.1: ${day081(list)}. Day08.2: ${day082(list)}.`;
}
