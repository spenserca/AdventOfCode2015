'use strict';

const day082 = require('../day08.2');
const fs = require('fs');
const input = fs.readFileSync('./Day08/spec/testInput.txt', 'utf8');

describe('Day08.2', function () {

    it('should return 19 for the given input', function () {
        expect(day082(input)).toEqual(19);
    });
});