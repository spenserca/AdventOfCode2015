'use strict';

const day081 = require('../day08.1');
const fs = require('fs');
const input = fs.readFileSync('./Day08/spec/testInput.txt', 'utf8');

describe('Day08.1', function () {

    it('should return 12 for the given input', function () {
        expect(day081(input)).toEqual(12);
    });
});