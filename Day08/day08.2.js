'use strict';

module.exports = (input) => {
    const list = input.split('\r\n');

    let total = list.reduce((sum, current) => {
        let encoded = getEncodedString(current);

        return sum + (encoded.length - current.length);
    }, 0);

    return total;
}

const getEncodedString = (string) => {
    let encoded = string.replace(/\\/g, '\\\\')
        .replace(/(")/g, '\\"');

    return `"${encoded}"`;
};