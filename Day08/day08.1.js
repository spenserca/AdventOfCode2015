'use strict';

const fs = require('fs');
const def = fs.readFileSync('./Day08/input.txt', 'utf8').split('\r\n');

module.exports = (input) => {
    const list = input.split('\r\n');

    let total = list.reduce((sum, current) => {
        let inMemory = getInMemoryString(current);
        return sum + (current.length - (inMemory.length - 2));
    }, 0);

    return total;
}

const getInMemoryString = (string) => {
    return string.replace(/(\\")/g, '"')
        .replace(/(\\\\)/g, '/')
        .replace(/(\\x[a-fA-F0-9]{2})/g, (str) => {
            return String.fromCharCode(parseInt(str.substring(2), 16)); // Returns ASCII value from hex
        });
};