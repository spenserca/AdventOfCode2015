'use strict';

const day171 = require('../day17.1');

describe('Day17.1', function () {

    it('should return 4 for the given input', function () {
        let containers = '20\n15\n10\n5\n5';
        let litersToStore = 25;
        expect(day171(containers, litersToStore)).toEqual(4);
    });
});