'use strict';

const day041 = require('../day04.1');

xdescribe('Day04.1', function () {

    it('should return 609043 for abcdef', function () {
        expect(day041('abcdef')).toEqual(609043);
    });

    it('should return 1048970 for pqrstuv', function () {
        expect(day041('pqrstuv')).toEqual(1048970);
    });
});