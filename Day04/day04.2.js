'use strict';

const crypto = require('crypto');

module.exports = function (input) {
    const key = input;
    let count = 0;
    let hash = '';

    while (!hash.startsWith('000000')) {
        let valueToHash = key + count;
        hash = crypto.createHash('md5').update(valueToHash).digest("hex");
        count++;
    }

    return count - 1;
}