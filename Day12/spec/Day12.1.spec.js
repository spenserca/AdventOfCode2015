'use strict';

const day121 = require('../Day12.1');

describe('Day12.1', function () {

    it('should return 6 for [1, 2, 3]', function () {
        let input = {
            "a": [1, 2, 3]
        };
        expect(day121(input)).toEqual(6);
    });

    it('should return 6 for [1, 2, 3]', function () {
        let input = { "a": 2, "b": 4 };
        expect(day121(input)).toEqual(6);
    });

    it('should return 3 for [[[3]]]', function () {
        let input = {
            "a": [[[3]]]
        };
        expect(day121(input)).toEqual(3);
    });

    it('should return 3 for {"a":{"b":4},"c":-1}', function () {
        let input = {
            "a": {
                "b": 4
            },
            "c": -1
        };
        expect(day121(input)).toEqual(3);
    });

    it('should return 0 for []', function () {
        let input = { "a": [] };
        expect(day121(input)).toEqual(0);
    });

    it('should return 0 for {}', function () {
        let input = {};
        expect(day121(input)).toEqual(0);
    });
});