'use strict';

const day122 = require('../day12.2');

describe('Day12.2', function () {

    it('should return 12 for input', function () {
        let input = { "a": 3, "b": 5, "c": 4 };
        expect(day122(input)).toEqual(12);
    });

    it('should return 12 for input', function () {
        let input = { "a": "3", "b": 5, "c": "4", "d": "d" };
        expect(day122(input)).toEqual(12);
    });

    it('should return 6 for [1, 2, 3]', function () {
        let input = { "a": [1, 2, 3] };
        expect(day122(input)).toEqual(6);
    });

    it('should return 4 for [1,{"c":"red","b":2},3]', function () {
        let input = { "a": [1, { "c": "red", "b": 2 }, 3] };
        expect(day122(input)).toEqual(4);
    });

    it('should return 0 for {"d":"red","e":[1,2,3,4],"f":5}', function () {
        let input = { "d": "red", "e": [1, 2, 3, 4], "f": 5 };
        expect(day122(input)).toEqual(0);
    });

    it('should return 6 for [1,"red",5]', function () {
        let input = { "a": [1, "red", 5] };
        expect(day122(input)).toEqual(6);
    });

    it('should return 105 for input', function () {
        let input = {
            "a": {
                "ab": {
                    "bc": [5, "red", 0],
                    "bd": "abc"
                },
                "ac": {
                    "ca": "red",
                    "cb": 10
                },
                "ad": 100
            }
        };
        expect(day122(input)).toEqual(105);
    });
});