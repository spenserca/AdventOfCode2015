'use strict';

const day121 = require('./Day12.1');
const day122 = require('./day12.2');
const input = require('./input.json');

module.exports = () => {
    return `Day12.1: ${day121(input)}. Day12.2: ${day122(input)}.`;
}