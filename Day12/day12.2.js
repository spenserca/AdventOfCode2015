'use strict';

module.exports = (input) => {
    const object = input;

    let valuesToSum = traverse(object);
    return valuesToSum.reduce((sum, current) => {
        return sum + current;
    }, 0);
}

const traverse = (obj) => {
    let values = [];
    let keys = Object.keys(obj);

    if (keys && !containsRed(obj)) {
        keys.forEach(k => {
            if (isNumber(obj[k])) {
                values.push(obj[k]);
            }
            else if (isString(obj[k])) {
                parseInt(obj[k]) ? values.push(parseInt(obj[k])) : 0;
            }
            else if (isAnArray(obj[k])) {
                let objValues = obj[k];
                values = values.concat(traverseArray(objValues));
            }
            else if (isObject(obj[k])) {
                values = values.concat(traverse(obj[k]));
            }
        });
    }

    return values;
}

const containsRed = (obj) => {
    let keys = Object.keys(obj);
    let hasRed = false;

    keys.forEach(k => {
        if (!hasRed) {
            hasRed = obj[k] === "red";
        }
    });
    return hasRed;
}

const isNumber = (val) => {
    return typeof val === 'number';
}

const isString = (val) => {
    return typeof val === 'string';
}

const isAnArray = (val) => {
    return Array.isArray(val);
}

const isObject = (val) => {
    return typeof val === 'object';
}

const traverseArray = (arr) => {
    let values = [];
    arr.forEach(el => {
        if (isNumber(el) || isString(el)) {
            parseInt(el) ? values.push(parseInt(el)) : 0;
        }
        else if (isAnArray(el)) {
            values = values.concat(traverseArray(el));
        }
        else if (isObject(el)) {
            values = values.concat(traverse(el));
        }
    });
    return values;
}