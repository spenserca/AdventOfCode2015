'use strict';

module.exports = (input) => {
    const intValues = JSON.stringify(input)
        .match(/([0-9]{1,})|(\-[0-9]{1,})/g);

    let total = 0;
    if (intValues) {
        total = intValues.reduce((sum, current) => {
            return sum + parseInt(current);
        }, 0);
    }

    return total;
}