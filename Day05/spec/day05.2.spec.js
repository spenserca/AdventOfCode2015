'use strict';

const day052 = require('../day05.2');

describe('Day05.2', function () {

    it('should return 1 for qjhvhtzxzqqjkmpb', function () {
        expect(day052('qjhvhtzxzqqjkmpb')).toEqual(1);
    });

    it('should return 1 for xxyxx', function () {
        expect(day052('xxyxx')).toEqual(1);
    });

    it('should return 0 for uurcxstgmygtbstg', function () {
        expect(day052('uurcxstgmygtbstg')).toEqual(0);
    });

    it('should return 0 for ieodomkazucvgmuy', function () {
        expect(day052('ieodomkazucvgmuy')).toEqual(0);
    });

    it('should return 1 for yzsmlbnftftgwadz', function () {
        expect(day052('yzsmlbnftftgwadz')).toEqual(1);
    });
});