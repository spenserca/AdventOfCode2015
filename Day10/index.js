'use strict';

const day101 = require('./day10.1');
const fs = require('fs');
const input = fs.readFileSync('./Day10/input.txt', 'utf8');

module.exports = () => {
    return `Day10.1: ${day101(input)}.`;
}