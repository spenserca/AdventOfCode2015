'use strict';

const day101 = require('../day10.1');

xdescribe('Day10.1', function () {

    it('should return 2 for 1', function () {
        expect(day101('1')).toEqual(2);
    });

    it('should return 2 for 11', function () {
        expect(day101('11')).toEqual(2);
    });

    it('should return 4 for 21', function () {
        expect(day101('21')).toEqual(4);
    });

    it('should return 6 for 1211', function () {
        expect(day101('1211')).toEqual(6);
    });

    it('should return 6 for 111221', function () {
        expect(day101('111221')).toEqual(6);
    });
});