'use strict';

module.exports = (input) => {
    const instructions = input.split('\n')
        .map(i => {
            return {
                instruction: i.match(/(on)|(off)|(toggle)/g)[0],
                startingPosition: i.match(/([0-9]{1,3},[0-9]{1,3})/g)[0],
                endPosition: i.match(/([0-9]{1,3},[0-9]{1,3})/g)[1]
            }
        });

    let grid = [];
    for (let x = 0; x < 1000; x++) {
        let row = [];
        for (let y = 0; y < 1000; y++) {
            row.push({
                x: x,
                y: y,
                isOn: false
            });
        }
        grid.push(row);
    }

    instructions.forEach(instruction => {
        let startX = parseInt(instruction.startingPosition.split(',')[0]);
        let startY = parseInt(instruction.startingPosition.split(',')[1]);
        let endX = parseInt(instruction.endPosition.split(',')[0]);
        let endY = parseInt(instruction.endPosition.split(',')[1]);

        for (let x = startX; x <= endX; x++) {
            for (let y = startY; y <= endY; y++) {
                switch (instruction.instruction) {
                    case 'on':
                        grid[x][y].isOn = true;
                        break;
                    case 'off':
                        grid[x][y].isOn = false;
                        break;
                    case 'toggle':
                        grid[x][y].isOn = !grid[x][y].isOn
                        break;
                }
            }
        }
    });

    let lightsOn = 0;
    for (let i = 0; i < grid.length; i++) {
        lightsOn += grid[i].filter(light => light.isOn).length;
    }

    return lightsOn;
}