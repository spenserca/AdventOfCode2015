'use strict';

module.exports = function (input) {
    const directions = input;
    let housesVisited = new Set();
    let sX = 0, sY = 0;
    let rX = 0, rY = 0;
    housesVisited.add(`(${sX},${sY})`);

    for (let i = 0; i < directions.length; i++) {
        let isRobotsTurn = i % 2 === 0;
        switch (directions[i]) {
            case '^':
                isRobotsTurn ? rY += 1 : sY += 1;
                break;
            case '>':
                isRobotsTurn ? rX += 1 : sX += 1;
                break;
            case 'v':
                isRobotsTurn ? rY -= 1 : sY -= 1;
                break;
            case '<':
                isRobotsTurn ? rX -= 1 : sX -= 1;
                break;
            default:
                isRobotsTurn ? (rX += 0, rY += 0) : (sX += 0, sY += 0);
        }

        isRobotsTurn
            ? housesVisited.add(`(${rX},${rY})`)
            : housesVisited.add(`(${sX},${sY})`);
    }

    return housesVisited.size;
}