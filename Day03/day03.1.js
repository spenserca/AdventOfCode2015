'use strict';

module.exports = function (input) {
    const directions = input;
    let x = 0, y = 0;
    let housesVisited = new Set();
    housesVisited.add(`(${x},${y})`);

    for (let i = 0; i < directions.length; i++) {
        switch (directions[i]) {
            case '>':
                x += 1;
                break;
            case '<':
                x -= 1;
                break;
            case '^':
                y += 1;
                break;
            case 'v':
                y -= 1;
                break;
            default:
                x += 0, y += 0;
        }
        housesVisited.add(`(${x},${y})`);
    }

    return housesVisited.size;
}