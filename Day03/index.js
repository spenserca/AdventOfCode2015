'use strict';

const day031 = require('./day03.1');
const day032 = require('./day03.2');
const fs = require('fs');
const input = fs.readFileSync('./Day03/input.txt', 'utf8');

module.exports = function () {
    return `Day03.1: ${day031(input)}. Day03.2: ${day032(input)}.`;
};