'use strict';

module.exports = (input, key) => {
    const circuitOperations = input.split('\n')
        .map(i => {
            let operation = i.split(' ');
            if (operation.includes('NOT')) {
                return {
                    first: operation[1],
                    operator: operation[0],
                    target: operation[operation.length - 1]
                };
            }
            else if (!operation.includes('AND') && !operation.includes('OR') && !operation.includes('LSHIFT') && !operation.includes('RSHIFT')) {
                return {
                    first: operation[0],
                    target: operation[operation.length - 1]
                };
            }
            else {
                return {
                    first: parseInt(operation[0]) ? parseInt(operation[0]) : operation[0],
                    second: parseInt(operation[2]) ? parseInt(operation[2]) : operation[2],
                    operator: operation[1],
                    target: operation[operation.length - 1]
                };
            }
        });
    console.log(circuitOperations);

    let wires = [];
    wires = circuitOperations.map(op => {
        let wire = wires.find(w => w.name === op.target);
        if (!wire) {
            return {
                name: op.target,
                value: 0
            };
        }
    });

    circuitOperations.forEach(op => {
        wires.find(w => w.name === op.target).value += getBitwiseValue(op, wires);
    });

    return wires.find(w => w.name === key).value;
}

const getBitwiseValue = (operation, wires) => {
    let first = wires.find(w => w.name === operation.first);
    if (!first) {
        first = operation.first;
    }
    let second = wires.find(w => w.name === operation.second);
    if (!second) {
        second = operation.second;
    }

    switch (operation.operator) {
        case 'AND':
            return parseInt(first & second)
                ? parseInt(first & second)
                : (first & second);
            break;
        case 'OR':
            return parseInt(first | second)
                ? parseInt(first | second)
                : (first | second);
            break;
        case 'LSHIFT':
            return parseInt(first << second)
                ? parseInt(first << second)
                : (first << second);
            break;
        case 'RSHIFT':
            return parseInt(first >> second)
                ? parseInt(first >> second)
                : (first >> second);
            break;
        case 'NOT':
            return parseInt(~first)
                ? parseInt(~first)
                : (~first);
            break;
        default:
            let wire = wires.find(w => w.name === first);
            return wire
                ? parseInt(wire.value)
                : parseInt(first);
            break;
    }
}