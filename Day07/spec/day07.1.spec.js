'use strict';

const day071 = require('../day07.1');
const input =
    `123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i`

xdescribe('Day07.1', function () {

    it('should return 72 for wire d after the input is run', function () {
        let result = day071(input, 'd');
        expect(result).toEqual(72);
    });

    it('should return 507 for wire e after the input is run', function () {
        let result = day071(input, 'e');
        expect(result).toEqual(507);
    });

    it('should return 492 for wire f after the input is run', function () {
        let result = day071(input, 'f');
        expect(result).toEqual(492);
    });

    it('should return 114 for wire g after the input is run', function () {
        let result = day071(input, 'g');
        expect(result).toEqual(114);
    });

    it('should return 65412 for wire h after the input is run', function () {
        let result = day071(input, 'h');
        expect(result).toEqual(65412);
    });

    it('should return 65079 for wire i after the input is run', function () {
        let result = day071(input, 'i');
        expect(result).toEqual(65079);
    });

    it('should return 123 for wire x after the input is run', function () {
        let result = day071(input, 'x');
        expect(result).toEqual(123);
    });

    it('should return 456 for wire y after the input is run', function () {
        let result = day071(input, 'y');
        expect(result).toEqual(456);
    });
});