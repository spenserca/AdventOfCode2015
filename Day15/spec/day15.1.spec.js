'use strict';

const day151 = require('../day15.1');

describe('Day15.1', function () {

    it('should return 62842880 for the given input', function () {
        const input = 'Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\nCinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3';
        expect(day151(input)).toEqual(62842880);
    });
});