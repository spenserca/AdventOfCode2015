'use strict';

module.exports = (input) => {
    const teaspoons = 100;
    const ingredients = input.split('\n')
        .map(line => line.split(' '))
        .map(split => {
            return {
                name: split[0].replace(':', ''),
                capacity: parseInt(split[2].replace(',', '')),
                durability: parseInt(split[4].replace(',', '')),
                flavor: parseInt(split[6].replace(',', '')),
                texture: parseInt(split[8].replace(',', '')),
                calories: parseInt(split[10])
            };
        });

    return 0;
}