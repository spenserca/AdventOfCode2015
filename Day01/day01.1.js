'use strict';

module.exports = function (input) {
    let floor = 0;

    for (let i = 0; i < input.length; i++) {
        switch (input[i]) {
            case '(':
                floor += 1;
                break;
            case ')':
                floor -= 1;
                break;
            default:
                floor += 0;
                break;
        }
    }

    return floor;
};