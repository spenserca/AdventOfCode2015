'use strict';

const day011 = require('./day01.1');
const day012 = require('./day01.2');
const fs = require('fs');
const input = fs.readFileSync('./Day01/input.txt', 'utf8');

module.exports = function () {
    return `Day01.1: ${day011(input)}. Day01.2: ${day012(input)}.`;
};