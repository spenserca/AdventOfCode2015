'use strict';

const day012 = require('../day01.2');

describe('Day01.2', function () {

    beforeEach(function () {
        this.day012 = day012;
    });

    it('should return 1 for )', function () {
        expect(this.day012(')')).toEqual(1);
    });

    it('should return 5 for ()())', function () {
        expect(this.day012('()())')).toEqual(5);
    });
});