'use strict';

const day011 = require('../day01.1');

describe('Day01.1', function () {

    beforeEach(function () {
        this.day011 = day011;
    });

    it('should return 0 for (())', function () {
        expect(this.day011('(())')).toEqual(0);
    });

    it('should return 0 for ()()', function () {
        expect(this.day011('()()')).toEqual(0);
    });

    it('should return 3 for (((', function () {
        expect(this.day011('(((')).toEqual(3);
    });

    it('should return 3 for (()(()(', function () {
        expect(this.day011('(()(()(')).toEqual(3);
    });

    it('should return 3 for ))(((((', function () {
        expect(this.day011('))(((((')).toEqual(3);
    });

    it('should return -1 for ())', function () {
        expect(this.day011('())')).toEqual(-1);
    });

    it('should return -1 for ))(', function () {
        expect(this.day011('))(')).toEqual(-1);
    });

    it('should return -3 for )))', function () {
        expect(this.day011(')))')).toEqual(-3);
    });

    it('should return -3 for )())())', function () {
        expect(this.day011(')())())')).toEqual(-3);
    });
});